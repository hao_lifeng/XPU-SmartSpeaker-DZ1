#include  <stdio.h>
#include <curl/curl.h> //libcurl的头文件
#include "cJSON.h"//cJSON头文件
#include <stdlib.h>//malloc使用的头文件


int main(void)
{
    FILE* fp;
    

    //响应消息的地址
    char* response = NULL;
    //响应消息的长度
    size_t resplen = 0;
    //创建内存文件，写入数据时自动分配内存
    fp = open_memstream(&response, &resplen);
    if(fp == NULL)//打开失败，打印错误，退出
    {
        perror("open_memstream() failed");
        return EXIT_FAILURE;
    }
   

//初始化HTTP客户端
    CURL* curl = curl_easy_init();
    if(curl == NULL)
    {
        perror("curl_easy_init() failed");
        return EXIT_FAILURE;
    }

    //准备HTTP请求消息,设置API地址（URL）
    curl_easy_setopt(curl,CURLOPT_URL,"http://www.nmc.cn/f/rest/aqi/57127");
    //如果不指定写入的文件，Libcurl会把服务器响应消息中的内容打印到屏幕上
    //如果指定了，就写入文件
    curl_easy_setopt(curl,CURLOPT_WRITEDATA,fp);

    //发送HTTP请求消息,等待服务器的响应消息
    CURLcode error = curl_easy_perform(curl);
    if(error != CURLE_OK)
    {
        fprintf(stderr,"curl_easy_perform() failed: %s\n", curl_easy_strerror(error));
        curl_easy_cleanup(curl);
        return EXIT_FAILURE;
    }

    //释放HTTP客户端申请的资源
    curl_easy_cleanup(curl);
    //关闭内存文件
    fclose(fp);

    puts(response);

    //解析json字符串
    cJSON* json = cJSON_Parse(response);
    if(json == NULL)
    {
        const char* error_pos = cJSON_GetErrorPtr();
        if(error_pos != NULL)
        {
            fprintf(stderr, "Error before: %s\n", error_pos);
        }
        return EXIT_FAILURE;
    }

    cJSON* forecasttime = cJSON_GetObjectItemCaseSensitive(json, "forecasttime");

    cJSON* aqi = cJSON_GetObjectItemCaseSensitive(json, "aqi");

    cJSON* aq = cJSON_GetObjectItemCaseSensitive(json, "aq");

    cJSON* text = cJSON_GetObjectItemCaseSensitive(json, "text");

    cJSON* aqiCode = cJSON_GetObjectItemCaseSensitive(json, "aqiCode");

    printf("陕西汉中市空气质量查询:\n");
    printf("预测时间: %s\n", forecasttime->valuestring);
    printf("空气质量指数: %d\n", aqi->valueint);
    printf("空气质量: %d\n", aq->valueint);
    printf("级别: %s\n", text->valuestring);

    //释放json数据结构占用的内存
    cJSON_free(json);

    free(response);

    return EXIT_SUCCESS;
}